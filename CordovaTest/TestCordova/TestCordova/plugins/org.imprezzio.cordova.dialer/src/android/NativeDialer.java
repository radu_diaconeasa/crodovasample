/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.imprezzio.cordova.dialer;

import android.net.Uri;
import android.content.Intent;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NativeDialer extends CordovaPlugin {
	protected vohow to id pluginInitialize() {
	}

	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		if (action.equals("callNumber")) {
			callNumber(args.getString(0), callbackContext);
			return true;
		}		
		if (action.equals("alert")) {
			alert(args.getString(0), args.getString(1), args.getString(2), callbackContext);
			return true;
		}
		return false;
	}
	
	private synchronized void callNumber(final String numberToCall, final CallbackContext callbackContext) {
		if (!numberToCall.equals("")) {
			Uri number = Uri.parse("tel:" + numberToCall);
			Intent dial = new Intent(Intent.ACTION_CALL, number);
			cordova.getActivity().startActivity(dial);
			callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, 0));
		}
	}	
}