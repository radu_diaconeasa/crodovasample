﻿angular.module('angularApp.controllers', [])

.controller('AppCtrl', function ($scope, $ionicModal, $timeout, $http, loginService) {
    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function () {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function () {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function () {
       // console.log('Doing login', $scope.loginData);
        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        //$timeout(function () {
        //    $scope.closeLogin();
        //}, 1000);

        $scope.sendLogin($scope.loginData.username, $scope.loginData.password);
    };

    $scope.sendLogin = function (username, password) {
        var result = loginService.login(username, password);
        result
            .success(function (data, status, headers, config) {
                $scope.closeLogin();
                console.log('login success', data);
            })
            .error(function (data, status, headers, config) {
                navigator.notification.alert(data, null, 'Error', 'OK');
                console.log('login error', data);
            });
    }
})
.controller('Contacts', function ($scope) {
    $scope.contacts = [];
    $scope.age = 5;
    $scope.contactsSuccess = function (contacts) {
        angular.forEach(contacts, function (contact) {
            console.log('contacts:' + contact.displayName);
            if (!contact.displayName)
                return;

            var c = {
                displayName: contact.displayName
            };
            if (contact.phoneNumbers && contact.phoneNumbers.length > 0) {
                c.phoneNumber = contact.phoneNumbers[0].value;
                $scope.age = 7;
            }
            $scope.contacts.push(c);
        });
    };

    $scope.contactsError = function () {

    };

    var options = new ContactFindOptions();
    //options.filter = "Bob";
    options.multiple = true;
    options.desiredFields = [navigator.contacts.fieldType.id, navigator.contacts.fieldType.displayName, navigator.contacts.fieldType.phoneNumbers, navigator.contacts.fieldType.name];
    var fields = [navigator.contacts.fieldType.displayName, navigator.contacts.fieldType.phoneNumbers, navigator.contacts.fieldType.name];
    navigator.contacts.find(fields, $scope.contactsSuccess, $scope.contactsError, options);
})
.controller('PlaylistsCtrl', function ($scope) {
    $scope.playlists = [
      { title: 'Reggae', id: 1 },
      { title: 'Chill', id: 2 },
      { title: 'Dubstep', id: 3 },
      { title: 'Indie', id: 4 },
      { title: 'Rap', id: 5 },
      { title: 'Cowbell', id: 6 }
    ];
})

.controller('PlaylistCtrl', function ($scope, $stateParams) {
});