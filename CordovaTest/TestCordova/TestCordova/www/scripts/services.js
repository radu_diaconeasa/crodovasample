﻿var myModule = angular.module('angularApp');

myModule.service('loginService', function ($http) {

    var service = {
        url: 'http://23.251.135.191:85/VcServices/api/v1/mobile/account/login',
        username: '',
        password: '',
        token: '',
        login: function (username, password) {
            var promise = $http.post('http://23.251.135.191:85/VcServices/api/v1/mobile/account/login',
                { email: username, password: password })
                .success(function (data, status, headers, config) {
                    /* reply:
                        {
                            "token": "...",
                            "phoneNumber": "...",
                            "userGuid": "a3daba1d-44a2-418b-949c-e798917d5cc8",
                            "firstName": "Samsung",
                            "lastName": "S5"
                        }
                     */
                    console.log('success login', data);
                    service.token = data.token;
                    //return true;
                    //$scope.closeLogin();
                }).error(function (data, status, headers, config) {
                    console.log('error login', data);
                    //  return false;
                });
            return promise;
        }
    }

    // factory function body that constructs shinyNewServiceInstance
    return service;
});