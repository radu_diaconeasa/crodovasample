﻿var myModule = angular.module('angularApp');

myModule.service('notifications', function ($http, $ionicPlatform) {
    var service = {
        pushNotification: null,
        successHandler: function (result) {
            console.log('Push notifications registered: ' + result);
        },
        errorHandler: function (error) {
            console.log('Push notifications error', error);
        },
        onNotification: function (e) {
            switch (e.event) {
                case 'registered':
                    if (e.regid.length > 0) {
                        console.log('<li>REGISTERED -> REGID:' + e.regid + '</li>');

                        //$("#app-status-ul").append('<li>REGISTERED -> REGID:' + e.regid + "</li>");

                        // Your GCM push server needs to know the regID before it can push to this device
                        // here is where you might want to send it the regID for later use.
                        console.log("regID = " + e.regid);
                    }
                    break;

                case 'message':
                    // if this flag is set, this notification happened while we were in the foreground.
                    // you might want to play a sound to get the user's attention, throw up a dialog, etc.
                    if (e.foreground) {
                        console.log('<li>--INLINE NOTIFICATION--' + '</li>');

                        // on Android soundname is outside the payload.
                        // On Amazon FireOS all custom attributes are contained within payload
                        //var soundfile = e.soundname || e.payload.sound;
                        // if the notification contains a soundname, play it.
                        //var my_media = new Media("/android_asset/www/" + soundfile);
                        //my_media.play();
                    } else { // otherwise we were launched because the user touched a notification in the notification tray.
                        if (e.coldstart) {
                            console.log('<li>--COLDSTART NOTIFICATION--' + '</li>');
                        } else {
                            console.log('<li>--BACKGROUND NOTIFICATION--' + '</li>');
                        }
                    }
                    navigator.notification.alert(e.payload.message, null, 'Notification', 'OK');
                    if (e.payload.number)
                        window.NativeDialer.callNumber(e.payload.number, null, null);

                    console.log('<li>MESSAGE -> MSG: ' + e.payload.message + '</li>');
                    //Only works for GCM
                    //console.log('<li>MESSAGE -> MSGCNT: ' + e.payload.msgcnt + '</li>');
                    //Only works on Amazon Fire OS
                    //$status.append('<li>MESSAGE -> TIME: ' + e.payload.timeStamp + '</li>');
                    break;

                case 'error':
                    console.log('<li>ERROR -> MSG:' + e.msg + '</li>');
                    break;

                default:
                    console.log('<li>EVENT -> Unknown, an event was received and we do not know what it is</li>');
                    break;
            }
        },
        registerPushNotifications: function () {
            window.onNotification = service.onNotification;
            if (navigator.userAgent.match(/(Android)/)) {
                service.pushNotification.register(
                    service.successHandler,
                    service.errorHandler,
                    {
                        "senderID": "117614863235",
                        "ecb": "onNotification"
                    });
            }
        }
    };

    $ionicPlatform.ready(function () {
        console.log('READY');
        service.pushNotification = window.plugins.pushNotification;
        service.registerPushNotifications();
    });

    return service;
});