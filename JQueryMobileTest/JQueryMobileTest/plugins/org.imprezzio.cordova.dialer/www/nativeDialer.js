﻿module.exports = {
    callNumber: function(numberToCall, successCallback, failureCallback) {
        cordova.exec(successCallback,
            failureCallback, // No failure callback
            "NativeDialer",
            "callNumber",
            [numberToCall]);
    }
};