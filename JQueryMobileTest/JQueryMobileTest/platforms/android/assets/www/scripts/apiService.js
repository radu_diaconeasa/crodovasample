﻿define(["jquery"],
    function($) {
        var service = {
            url: 'http://23.251.135.191:85/VcServices/api/v1/mobile/account/login',
            username: '',
            password: '',
            token: '',

            login: function(username, password) {
                var promise = $.ajax({
                    url: 'http://23.251.135.191:85/VcServices/api/v1/mobile/account/login',
                    data: { email: username, password: password },
                    type: 'POST',
                }).done(function(data) {
                    /* reply:
                        {
                            "token": "...",
                            "phoneNumber": "...",
                            "userGuid": "a3daba1d-44a2-418b-949c-e798917d5cc8",
                            "firstName": "Samsung",
                            "lastName": "S5"
                        }
                     */
                    service.token = data.token;
                    return true;
                }).fail(function() {
                    return false;
                });
                return promise;
            }
        };

        return service;
    });