﻿define(['jquery', 'underscore', 'backbone', 'text!./createCall.html', 'text!./contactItem.html'],
    function($, _, Backbone, viewTemplate, contactTemplate) {
        var ContactModel = Backbone.Model.extend({});

        var ContactView = Backbone.View.extend({
            tagName: "li",
            template: _.template(contactTemplate),
            render: function() {
                var json = this.model.toJSON();
                var renderedTemplate = this.template(json);
                this.$el.html(renderedTemplate);
                return this;
            }
        });

        var ContactsCollection = Backbone.Collection.extend({
            model: ContactModel
        });

        var view = Backbone.View.extend({
            template: _.template(viewTemplate),
            listId: 'contactsList',
            initialize: function() {
                _.bindAll(this, 'contactOnSuccess', 'contactOnError', 'render', 'addContact');

                this.model = new ContactsCollection();
                this.model.bind('add', this.addContact);

                var options = new ContactFindOptions();
                var fields = ["displayName", "name", "phoneNumbers"];
                options.multiple = true;
                navigator.contacts.find(fields, this.contactOnSuccess, this.contactOnError, options);
            },
            contactOnSuccess: function(contacts) {
                for (var i = 0; i < contacts.length; i++) {
                    var contact = contacts[i];
                    if (!contact.phoneNumbers)
                        continue;

                    this.model.add(
                        new ContactModel(
                        {
                            displayName: contact.displayName,
                            name: contacts[i].name,
                            numbers: contacts[i].phoneNumbers
                        }));
                }
            },
            contactOnError: function() {

            },
            addContact: function(model, collection, options) {
                var contactView = new ContactView({ model: model });
                var rendered = contactView.render();
                $('#' + this.listId).append(rendered.el);
                $('#' + this.listId).listview('refresh');
            },
            render: function() {
                var that = this;
                this.$el.html(this.template);
                setTimeout(function() {
                    that.addContact(new ContactModel(
                    {
                        displayName: 'contact 1',
                        name: 'test contact name',
                        numbers:
                        [
                            { type: 'mobile', value: '90740041209' },
                            { type: 'work', value: '0740041092' }
                        ]
                    }));
                    that.addContact(new ContactModel(
                    {
                        displayName: 'contact 2',
                        name: 'test contact name',
                        numbers:
                        [
                            { type: 'mobile', value: '90740041209' },
                            { type: 'work', value: '0740041092' }
                        ]
                    }));
                }, 1000);

                return this;
            }
        });

        return view;
    });