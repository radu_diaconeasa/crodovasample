﻿define(["jquery", "backbone", "scripts/views/main/main", "scripts/views/login/login", "scripts/views/mainMenu/mainMenu", "scripts/views/createCall/createCall"],
    function($, Backbone, MainView, LoginView, MainMenuView, CreateCallView) {
        // Extends Backbone.Router
        var router = Backbone.Router.extend({
            firstPage: true,
            lastPage: null,
            lastRoute: "",

            // The Router constructor
            initialize: function() {
                $.mobile.pageContainer.on('pagecontainerhide', this.pageHidden);
                // Tells Backbone to start watching for hashchange events
                Backbone.history.start();
            },

            // Backbone.js Routes
            routes: {
                // When there is no hash bang on the url, the home method is called
                "": "index",
                "back": "back",
                "login": "login",
                "mainMenu": "mainMenu",
                "createCall": "createCall",
                // When #category? is on the url, the category method is called
                "category?:type": "category"
            },

            back: function() {
                $.mobile.pageContainer.pagecontainer("change", router.lastPage, { changeHash: false, reverse: true });
            },

            index: function() {
                this.changePage(new MainView());
            },
            login: function() {
                this.changePage(new LoginView());
            },
            mainMenu: function() {
                this.changePage(new MainMenuView());
            },
            createCall: function() {
                this.changePage(new CreateCallView());
            },
            changePage: function(page) {
                $(page.el).attr('data-role', 'page');
                page.render();
                $('body').append($(page.el));
                var transition = $.mobile.defaultPageTransition;
                // We don't want to slide the first page
                if (this.firstPage) {
                    transition = 'none';
                    this.firstPage = false;
                }
                $.mobile.pageContainer.pagecontainer("change", $(page.el), { changeHash: false, transition: transition });
            },
            pageHidden: function(event, ui) {
                //detach hidden page from DOM
                router.lastPage = ui.prevPage;
                //ui.prevPage.detach();
            }
        });

        // Returns the Router class
        return router;
    });