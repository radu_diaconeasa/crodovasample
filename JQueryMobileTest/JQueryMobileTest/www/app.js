﻿// Sets the require.js configuration for your application.
require.config({

    // 3rd party script alias names (Easier to type "jquery" than "libs/jquery-1.8.2.min")
    paths: {
        // Core Libraries
        "jquery": "libs/jquery-1.11.2/jquery-1.11.2.min",
        "text": "libs/require/text", // RequireJS plugin
        //"domReady": 'libs/require/domReady', // RequireJS plugin  - NOT USED
        "jquerymobile": "libs/jquery.mobile-1.4.5/jquery.mobile-1.4.5",
        "underscore": "libs/underscore/1.8.3/underscore",
        "backbone": "libs/backbone/1.1.2/backbone",
        "apiService": "scripts/apiService",
       // "jscompat": "scripts/winstore-jscompat"
    },

    // Sets the configuration for your third party scripts that are not AMD compatible
    shim: {
        "backbone": {
            "deps": ["underscore", "jquery"],//, "jscompat"],
            "exports": "Backbone" //attaches "Backbone" to the window object
        },
        underscore: {
            exports: '_'
        }
    } // end Shim Configuration
});
// Includes File Dependencies
require(["jquery", "backbone", "scripts/router"], function($, Backbone, Router) {
    $(document).on("mobileinit",
        // Set up the "mobileinit" handler before requiring jQuery Mobile's module
        function() {
            // Prevents all anchor click handling including the addition of active button state and alternate link bluring.
            $.mobile.linkBindingEnabled = false;
            // Disabling this will prevent jQuery Mobile from handling hash changes
            $.mobile.hashListeningEnabled = false;
            $.mobile.ajaxEnabled = false;
            $.mobile.pushStateEnabled = false;

            $.mobile.defaultPageTransition = "slide";
        }
    );

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
        document.addEventListener("backbutton", onBackKeyDown.bind(this), false);

        pushNotification = window.plugins.pushNotification;
        registerPushNotifications();

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
    };

    function successHandler(result) {
        var x = 10;
        console.log('result = ' + result);
    }

    function errorHandler(error) {
        alert('error = ' + error);
    }

    function onNotification(e) {
        switch (e.event) {
        case 'registered':
            if (e.regid.length > 0) {
                console.log('<li>REGISTERED -> REGID:' + e.regid + '</li>');

                //$("#app-status-ul").append('<li>REGISTERED -> REGID:' + e.regid + "</li>");

                // Your GCM push server needs to know the regID before it can push to this device
                // here is where you might want to send it the regID for later use.
                console.log("regID = " + e.regid);
            }
            break;

        case 'message':
            // if this flag is set, this notification happened while we were in the foreground.
            // you might want to play a sound to get the user's attention, throw up a dialog, etc.
            if (e.foreground) {
                console.log('<li>--INLINE NOTIFICATION--' + '</li>');

                // on Android soundname is outside the payload.
                // On Amazon FireOS all custom attributes are contained within payload
                //var soundfile = e.soundname || e.payload.sound;
                // if the notification contains a soundname, play it.
                //var my_media = new Media("/android_asset/www/" + soundfile);
                //my_media.play();
            } else { // otherwise we were launched because the user touched a notification in the notification tray.
                if (e.coldstart) {
                    console.log('<li>--COLDSTART NOTIFICATION--' + '</li>');
                } else {
                    console.log('<li>--BACKGROUND NOTIFICATION--' + '</li>');
                }
            }

            console.log('<li>MESSAGE -> MSG: ' + e.payload.message + '</li>');
            //Only works for GCM
            //console.log('<li>MESSAGE -> MSGCNT: ' + e.payload.msgcnt + '</li>');
            //Only works on Amazon Fire OS
            //$status.append('<li>MESSAGE -> TIME: ' + e.payload.timeStamp + '</li>');
            break;

        case 'error':
            console.log('<li>ERROR -> MSG:' + e.msg + '</li>');
            break;

        default:
            console.log('<li>EVENT -> Unknown, an event was received and we do not know what it is</li>');
            break;
        }
    }

    function registerPushNotifications() {
        window.onNotification = onNotification;
        if (navigator.userAgent.match(/(Android)/)) {
            //device.platform == 'android' || device.platform == 'Android' || device.platform == "amazon-fireos") {
            pushNotification.register(
                successHandler,
                errorHandler,
                {
                    "senderID": "117614863235",
                    "ecb": "onNotification"
                });
        }
    };

    function onBackKeyDown() {
        window.Router.back();
        console.log('back');
    };

    function onPause() {
        console.log('onPause');
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        console.log('onResume');
        // TODO: This application has been reactivated. Restore application state here.
    };

    require(["jquerymobile"], function() {
        // Instantiates a new Backbone.js Mobile Router
       // this.router = new Router();

        // Prevents all anchor click handling including the addition of active button state and alternate link bluring.
        $.mobile.linkBindingEnabled = false;
        // Disabling this will prevent jQuery Mobile from handling hash changes
        $.mobile.hashListeningEnabled = false;
        $.mobile.ajaxEnabled = false;
        $.mobile.pushStateEnabled = false;

        $.mobile.defaultPageTransition = "slide";

        window.Router = new Router();
    });
})();