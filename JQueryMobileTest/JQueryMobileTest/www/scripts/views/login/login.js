﻿define(['jquery', 'underscore', 'backbone', 'apiService', 'text!./login.html'],
    function($, _, backbone, api, loginTemplate) {
        var loginView = backbone.View.extend({
            template: _.template(loginTemplate),
            events: {
                'click button#btn-submit': 'submit'
            },
            render: function() {
                console.log('calling number');
                //NativeDialer.callNumber('0740041092', this.callSuccess, this.callError);
                this.$el.html(loginTemplate);
                return this;
            },
            callSuccess: function() {
                console.log('success calling number');
            },
            callError: function(er) {
                console.log('error calling number' + er);
            },
            submit: function(e) {
                e.preventDefault();
                var that = this;
                var frm = $(this.$el.find('form'));
                var data = this.getFormData(frm);
                var result = api.login(data.email, data.password);
                result.done(function() {
                    $.mobile.navigate("mainMenu");
                }).fail(function() {
                    that.$el.find('#dlg-invalid-credentials').popup("open", { 'data-transition': 'fade' });
                });
            },
            getFormData: function($form) {
                var unindexed_array = $form.serializeArray();
                var indexed_array = {};

                $.map(unindexed_array, function(n, i) {
                    indexed_array[n['name']] = n['value'];
                });

                return indexed_array;
            }
        });

        return loginView;
    });