﻿define(['jquery', 'underscore', 'backbone', 'text!./mainMenu.html'],
    function ($, _, backbone, mainTemplate) {
        var mainView = backbone.View.extend({
            template: _.template(mainTemplate),
            render: function() {
                this.$el.html(mainTemplate);
                return this;
            }
        });

        return mainView;
    });